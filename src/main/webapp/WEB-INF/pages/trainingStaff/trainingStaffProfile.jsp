<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<section class="content-header">
	<h1>
		Dashboard <small>Data Infomation</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12 ">
			<div class="box box-info">
				<div class="box-header with-border">

					<section class="content-header" style="margin-bottom: 20px;">
						<h1>Information Training Staff</h1>
						<ol class="breadcrumb">
							<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
							<li class="active">Information Training Staff</li>
						</ol>
					</section>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				<form:form action="" class="form-horizontal" method="POST" commandName="entity">

					<div class="box-body">



						<div class="form-group">
							<div class="col-sm-3" style="text-align: right;">
								<form:label path="userNameTitle" for="inputUser"
									class="control-label">UserName:</form:label>
							</div>
							<div class="col-sm-6">
								<form:label path="userName" id="text-user" class="control-label">${entity.name}</form:label>
								<div id="edit-user">
									<div>
										<form:input path="userName" type="userName"
											class="form-control" id="inputUser" value="${entity.name}" />
									</div>
									<div class="col-sm-6 tms-form-btn-edit">
										<form:button type="submit" class="btn-flat btn bg-blue"
											id="idSaveUser">Save</form:button>
										<form:button id="hideEditUser" type="submit"
											class="btn-flat btn bg-gray">Cancel</form:button>
									</div>
								</div>
							</div>
							<div class="col-xs-3" style="text-align: center;">
								<!-- <button id="showEditUser" class="btn-link">Edit</button> -->
								<a id="showEditUser" class="btn-link" style="cursor: pointer">
									<span class="glyphicon glyphicon-pencil"></span>
								</a>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-3" style="text-align: right;">
								<form:label path="emailTitle" for="inputEmail"
									class=" control-label">Email:</form:label>
							</div>
							<div class="col-sm-6">
								<form:label path="email" id="text-email" class="control-label">${entity.email}</form:label>
								<div id="edit-email">

									<div>
										<form:input path="email" type="email" class="form-control"
											id="inputEmail" value="${entity.email}" />
									</div>


								</div>
							</div>

						</div>

						<div class="form-group">
							<div class="col-sm-3" style="text-align: right;">
								<form:label path="passwordTitle" for="inputPass"
									class=" control-label">Password:</form:label>
							</div>
							<div class="col-sm-6">
								<form:password path="password" id="text-pass"
									class="control-label">${entity.password}</form:password>
								>
								<div id="edit-pass">

									<div>
										<form:input path="password" type="password"
											class="form-control tms-form-btn-edit" id="inputPass"
											value="${entity.password}" />
									</div>

									<div>
										<form:input path="password" type="password"
											class="form-control tms-form-btn-edit" id="inputPass"
											placeholder="Enter a new password" />
									</div>

									<div>
										<form:input path="password" type="password"
											class="form-control tms-form-btn-edit " id="inputPass"
											placeholder="Enter a new password" />
									</div>

									<div class="col-sm-6 tms-form-btn-edit">
										<form:button type="submit" class="btn-flat btn bg-blue"
											id="idSavePassword">Save</form:button>
										<form:button id="hideEditPass" type="submit"
											class="btn-flat btn bg-gray">Cancel</form:button>
									</div>
								</div>
							</div>
							<div class="col-xs-3" style="text-align: center;">
								<!-- <button id="showEditPass" class="btn-link">Edit</button> -->
								<a id="showEditPass" class="btn-link" style="cursor: pointer">
									<span class="glyphicon glyphicon-pencil"></span>
								</a>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
				</form:form>
			</div>
		</div>
	</div>
</section>

<script>
	$(document).ready(function() {
		console.log("ACTION");
		$("#showEditUser").click(function() {
			$("#edit-user").show();
			$("#text-user").hide();
			console.log("SHOW");
		});

		$("#hideEditUser").click(function() {
			$("#edit-user").hide();
			$("#text-user").show();
			console.log("HIDE");
		});

	});
</script>


<script>
	$(document).ready(function() {
		console.log("ACTION");
		$("#showEditPass").click(function() {
			$("#edit-pass").show();
			$("#text-pass").hide();
			console.log("SHOW");
		});

		$("#hideEditPass").click(function() {
			$("#edit-pass").hide();
			$("#text-pass").show();
			console.log("HIDE");
		});
	});
</script>

<script>
	$(document).ready(function() {
		console.log("ACTION");
		$("#showEditEmail").click(function() {
			$("#edit-email").show();
			$("#text-email").hide();
			console.log("SHOW");
		});

		$("#hideEditEmail").click(function() {
			$("#edit-email").hide();
			$("#text-email").show();
			console.log("HIDE");
		});
	});
</script>

<%-- save User Name --%>
<script>
	jQuery(document).ready(function($) {

		$("#btn-save").click(function(event) {

			var id = $('#idSaveUser').val();
			$("#btn-save").prop("disabled", true);

			$.post("/path-to/hosting/save", {
				id : id,
			}, function(data) {
				var json = JSON.parse(data);
				//...

			}).done(function() {
			}).fail(function(xhr, textStatus, errorThrown) {
			}).complete(function() {
				$("#btn-save").prop("disabled", false);

			});

		});

	});
</script>
<%-- save Email --%>
<script>
	jQuery(document).ready(function($) {

		$("#btn-save").click(function(event) {

			var id = $('#idSaveEmail').val();
			$("#btn-save").prop("disabled", true);

			$.post("/path-to/hosting/save", {
				id : id,
			}, function(data) {
				var json = JSON.parse(data);
				//...

			}).done(function() {
			}).fail(function(xhr, textStatus, errorThrown) {
			}).complete(function() {
				$("#btn-save").prop("disabled", false);

			});

		});

	});
</script>

<%-- save password --%>
<script>
	jQuery(document).ready(function($) {

		$("#btn-save").click(function(event) {

			var id = $('#idSavePassword').val();
			$("#btn-save").prop("disabled", true);

			$.post("/path-to/hosting/save", {
				id : id,
			}, function(data) {
				var json = JSON.parse(data);
				//...

			}).done(function() {
			}).fail(function(xhr, textStatus, errorThrown) {
			}).complete(function() {
				$("#btn-save").prop("disabled", false);

			});

		});

	});
</script>